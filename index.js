import nodemailer from 'nodemailer';
import mjml2html from 'mjml';
import { readFileSync } from 'fs';

let Eta = await import('eta');

Eta.configure({ tags: ['{{', '}}'] });

// async..await is not allowed in global scope, must use a wrapper
async function main() {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    let testAccount = await nodemailer.createTestAccount();
  
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.ethereal.email",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
      },
    });
  
    // Plugin to handle mjml
    transporter.use('compile', (mail, callback) => {
        if (mail.data.html || !mail.data?.template) return callback();
        if(!mail.data?.locals) mail.data.locals = {};

        const file = readFileSync(`./templates/${mail.data.template}.mjml`, 'utf-8');

        const html = mjml2html(file, {
            filePath: './templates'
        });
    
        mail.data.html = Eta.render(html.html, mail.data.locals);

        callback();
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        template: "email1",
        locals: {
            header: 'Sample Email',
            name: 'Daniel'  
        },
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: "bar@example.com, baz@example.com", // list of receivers
        subject: "Hello ✔", // Subject line
        // text: "Hello world?", // plain text body
        //html: "<b>Hello world?</b>", // html body
    });
  
    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  
    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  }
  
  main().catch(console.error);