import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __fullpath = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__fullpath);
const __filename = path.basename(__fullpath);


export default new Promise(res=>{
    fs.readdirSync(__dirname, (err, files) => {
        res(files.reduce(async (prev, cur)=>{
            return cur==__filename ? prev : await import(cur);
        }, {}));
    });
});